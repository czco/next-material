import Meta from '../components/meta'
import Footer from '../components/footer'
// import React                from 'react';
// import Head                 from 'next/head';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';


// // if (typeof window !== 'undefined') injectTapEventPlugin();
try { injectTapEventPlugin(); } catch (e) { }

const muiTheme = getMuiTheme({ userAgent: false });

export default ({ children }) => (
    <div>
        <Meta />
        <MuiThemeProvider muiTheme={muiTheme}>
            <div>
                <AppBar title="Page Title" iconClassNameRight="muidocs-icon-navigation-expand-more" />
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            {children}
                        </div>
                        
                    </div>
                </div>
                <Footer />
            </div>
        </MuiThemeProvider>
    </div>
)